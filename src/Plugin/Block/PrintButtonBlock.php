<?php

declare(strict_types=1);

namespace Drupal\partial_page_print\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the Print Button Block for printing part of the page.
 *
 * @Block(
 *   id="print_button_block",
 *   admin_label = @Translation("Print Button"),
 * )
 */
class PrintButtonBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return parent::defaultConfiguration() + [
      'print_button_block' => [],
    ];
  }

  /**
   * Overrides \Drupal\Core\Block\BlockBase::blockForm().
   *
   * Adds button title and element id fields to the block configuration form.
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['button_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Print Button Title'),
      '#default_value' => $config['button_title'] ?? '',
      '#required' => TRUE,
    ];

    $form['element_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Print Element ID'),
      '#default_value' => $config['element_id'] ?? '',
      '#required' => TRUE,
      '#description' => $this->t('Be sure that the element id is populated with the HTML ID of the section you want to be printed on the page. Add this without #.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();
    $this->configuration['button_title'] = $values['button_title'];
    $this->configuration['element_id'] = $values['element_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    return [
      'print_button' => [
        '#value' => $this->configuration['button_title'],
        '#type' => 'partial_page_print_button',
        '#element_id' => $this->configuration['element_id'],
      ],
    ];
  }

}
