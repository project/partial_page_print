<?php

declare(strict_types=1);

namespace Drupal\partial_page_print\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an empty page with theme assets to be used to print part of a page.
 */
class PartialPagePrintController implements ContainerInjectionInterface {

  /**
   * Constructs a PartialPagePrintController.
   */
  public function __construct() {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): PartialPagePrintController|static {
    return new static();
  }

  /**
   * Builds a page with a placeholder element for use by the partial print JS.
   *
   * @return array
   *   Render array with placeholder print element.
   *
   * @see html--print-document.html.twig
   * @see page--print-document.html.twig
   */
  public function build(): array {
    return [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'id' => 'print-placeholder',
      ],
      '#attached' => [
        'library' => [
          'partial_page_print/print_document',
        ],
      ],
    ];
  }

}
