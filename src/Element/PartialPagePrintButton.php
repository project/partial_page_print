<?php

declare(strict_types=1);

namespace Drupal\partial_page_print\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * Provides a render element to a button that prints part of a page.
 *
 * Properties:
 * - #value: The text to be shown on the button.
 * - #element_id: The ID of the element to print.
 *
 * Usage Example:
 * @code
 * $build['print_link'] = [
 *   '#value' => $this->t('Print the Content'),
 *   '#type' => 'partial_page_print_button',
 *   '#element_id' => 'my_div_id'
 * ];
 * @endcode
 *
 * @RenderElement("partial_page_print_button")
 */
class PartialPagePrintButton extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo(): array {
    return [
      '#pre_render' => [
        [self::class, 'preRenderPrintButton'],
      ],
      '#theme' => 'partial_page_print_button',
      '#content' => '',
      '#attributes' => [],
    ];
  }

  /**
   * Pre-render callback: Attaches the library and required markup.
   *
   * @param array $element
   *   Renderable array for '#type' => 'partial_page_print_button' element.
   *
   * @return array
   *   The renderable array.
   */
  public static function preRenderPrintButton(array $element): array {
    $element['#attached']['library'][] = 'partial_page_print/print_button';

    if (!empty($element['#element_id'])) {
      $element['#attributes']['data-print-element-id'] = $element['#element_id'];
    }

    if (!empty($element['#value'])) {
      $element['#content'] = $element['#value'];
    }

    return $element;
  }

}
