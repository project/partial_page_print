## Introduction

Introduces a button (render element) to allow visitors to print part of the
current page. The printed page would use the same theme but without any of the
regions, blocks, or other parts of the page that are not desired on the printed
page.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install with composer: `composer require drupal/partial_page_print` then enable
the module.

## Using

To add the print button to a page add the following element to the render array
in your custom module or with a preprocess hook in your theme. Be sure that
the `#element_id` is populated with the HTML ID of the thing you want to be
printed on the page.

```php
 $build['print_link'] = [
   '#value' => $this->t('Print the Content'),
   '#type' => 'partial_page_print_button',
   '#element_id' => 'my_div_id',
 ];
```

### Example
Example of adding the partial page print button to a node template in your
site's theme:

**In your theme's .theme file:**
```php
function function MYTHEME_preprocess_node(&$variables) {
 $variables['nodeSummaryPrintButton'] = [
   '#value' => $this->t('Print Summary'),
   '#type' => 'partial_page_print_button',
   '#element_id' => 'node-summary',
 ];
}
```

**In your theme's node template:**
```twig
<article{{ attributes }}>
  {# ... #}
  {{ nodeSummaryPrintButton }}
  <div id="node-summary">
    {# Node summary here #}
  </div>
  {# ... #}
</article>
```


## Maintainers

- Chris Snyder - https://www.drupal.org/u/ChrisSnyder
