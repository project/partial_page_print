(function (Drupal, drupalSettings, once) {
  const printElementFrameName = 'print_element_frame';
  let printIframe;
  const init = () => {
    // Create the iframe that we can use to place the element for printing.
    if (!document.getElementById(printElementFrameName)) {
      printIframe = document.createElement('iframe');
      printIframe.setAttribute('name', printElementFrameName);
      printIframe.setAttribute('id', printElementFrameName);
      printIframe.setAttribute('src', `${window.location.origin + drupalSettings.path.baseUrl + drupalSettings.path.pathPrefix}print-document`);
      // Using style property, instead of setAttribute(), to avoid issues with strict a strict Content Security Policy.
      // See https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/style-src
      printIframe.style.display = 'none';
      printIframe.style.position = 'absolute';
      printIframe.setAttribute('width', '1200');
      printIframe.setAttribute('height', '100');
      document.body.appendChild(printIframe);
      // The following window is needed to hide the iframe after printing is complete. Displaying it is required for safari compatibility
      window.onafterprint = (event) => printIframe.style.display = 'none';
    }
  };

  Drupal.behaviors.partialPagePrintInit = {
    attach: function (context, settings) {
      once('partialPagePrintInit', 'html', context).forEach(element => {
        init();
      });
    }
  };

  Drupal.behaviors.partialPagePrint = {
    attach: function (context, settings) {
      once('partialPagePrint', '[data-print-element-id]', context).forEach(element => {
        element.addEventListener('click', event => {
          const buttonText = event.target.innerHTML;
          const printElementId = event.target.getAttribute('data-print-element-id');
          const elementToPrint= document.getElementById(printElementId);
          if (!elementToPrint) {
            console.error('Unable to print element with id ' + printElementId + ' because it is not found in current document.');
            return;
          }

          // Update the button text.
          event.target.innerHTML = Drupal.t('Generating...');

          // Update the iframed document title to match current page title because this can be shown on the printed page in the header.
          window.frames[printElementFrameName].document.title = document.title;

          // Place the element in the iframe to print.
          window.frames[printElementFrameName].postMessage({printableElement: elementToPrint.innerHTML}, '*');

          // Give the browser a moment to render the element in the iframe then print.
          setTimeout(() => {
            // Safari will print empty pages if left as display: none, however the onbeforeprint listener will not reliably fire with the correct timing, so we set that here.
            printIframe.style.display = 'block';
            // Print the frame
            window.frames[printElementFrameName].window.focus();
            window.frames[printElementFrameName].window.print();

            // Restore button text.
            event.target.innerHTML = buttonText;
          }, 1000 );
        });
      });
    }
  };
})(Drupal, drupalSettings, once);
