(function (Drupal, once) {
  Drupal.behaviors.partialPagePrintDocument = {
    attach: function (context, settings) {
      once('partialPagePrintDocument', 'html', context).forEach(element => {
        window.addEventListener(
          "message",
          function (event) {
            if (event.origin === window.location.origin && !!event.data.printableElement) {
              document.getElementById('print-placeholder').innerHTML = event.data.printableElement;
            }
          },
          false
        );
      });
    }
  };
})(Drupal, once);
